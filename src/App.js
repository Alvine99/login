import React from 'react';
import Login from './component/Login';
import Daftar from './component/Daftar';
import { Route, Switch } from 'react-router-dom';

function App() {
  return (
    <div className="App">
    <Switch>
      <Route exact path="/" component={Login} />
      <Route path="/daftar" component={Daftar} />
    </Switch>
    </div>
  );
}

export default App;
